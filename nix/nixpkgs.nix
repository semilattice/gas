let
	tarball = fetchTarball {
		url = "https://github.com/NixOS/nixpkgs/archive/3fd03165aaa07cb10c81044174094fd9d8dc5b27.tar.gz";
		sha256 = "1bfwghss667wilim6v8mwcr78bwv0k6bsyid8j26xb6mclzy0dxh";
	};
in
	import tarball {}
