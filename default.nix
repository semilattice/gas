let
	nixpkgs = import ./nix/nixpkgs.nix;
	ghc = import ./nix/ghc.nix;
in
	nixpkgs.haskell.packages.${ghc}.mkDerivation {
		pname = "pijp";
		version = "0.0.0";
		src = ./.;
		buildDepends =
			let h = nixpkgs.haskell.packages.${ghc}; in
			import ./nix/dependencies.nix h;
	}
