let
	nixpkgs = import ./nix/nixpkgs.nix;
	ghc = import ./nix/ghc.nix;
	ghcWithPackages =
		nixpkgs.haskell.packages.${ghc}.ghcWithPackages (h:
			[h.cabal-install h.ghcid h.hoogle] ++
			import ./nix/dependencies.nix h
		);
in
	nixpkgs.stdenv.mkDerivation {
		name = "pijp-nix-shell";
		buildInputs = [ghcWithPackages];
	}
